package cat.escolapia.pmdm;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;


public class DemoScene2D extends ApplicationAdapter {
	// Classe actor
	public class ActorTest extends Actor {
		Texture texture = new Texture(Gdx.files.internal("badlogic.jpg"));
		float actorX = 0, actorY = 0;
        Integer direction = 1;
		public boolean started = false;

		public ActorTest(){
			setBounds(actorX,actorY, texture.getWidth(),texture.getHeight());
			addListener(new InputListener(){
				public boolean touchDown (InputEvent event,float x,float y,int pointer,    int button) {
					((ActorTest)event.getTarget()).started = !((ActorTest)event.getTarget()).started;
					return true;
				}
			});
		}



		@Override
		public void draw(Batch batch, float alpha){
			batch.draw(texture,actorX,actorY);
		}

		@Override
		public void act(float delta){
			if(started){
				actorX += 10 * direction;
                if (actorX >= this.getStage().getWidth() - this.texture.getWidth() || actorX <= 0) direction = direction * -1;
                setBounds(actorX,actorY, texture.getWidth(),texture.getHeight());
			}
		}
	}


	// App principal
	private Stage stage;

	@Override
	public void create() {
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);

		ActorTest actorTest = new ActorTest();
		actorTest.setTouchable(Touchable.enabled);
		stage.addActor(actorTest);
	}

	@Override
	public void render() {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

}
